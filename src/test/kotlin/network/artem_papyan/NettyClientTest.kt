package network.artem_papyan

import network.artem_papyan.domain.*
import network.artem_papyan.util.NettyClient
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.util.concurrent.TimeUnit

class NettyClientTest {

    @Test
    fun udpClientTest() {
        val googleRequest = this.generateDnsRequest("google.com")

        val result = NettyClient.sendUdp("1.1.1.1", googleRequest)

        Assertions.assertNotNull(result)

        val response = result.get(5, TimeUnit.SECONDS)

        Assertions.assertFalse(result.isCompletedExceptionally)
        Assertions.assertFalse(response.answers.isEmpty())
    }

    @Test
    fun tcpClientTest() {
        val yandexRequest = this.generateDnsRequest("yandex.ru")

        val result = NettyClient.sendTcp("1.1.1.1", yandexRequest)

        Assertions.assertNotNull(result)

        val response = result.get(5, TimeUnit.SECONDS)

        Assertions.assertFalse(result.isCompletedExceptionally)
        Assertions.assertFalse(response.answers.isEmpty())
    }

    private fun generateDnsRequest(hostname: String) = DNSPacket(
        DNSHeader(
            1,
            DNSHeaderFlags(
                QR.REQUEST,
                OPCode.STANDARD_QUERY,
                isAuthoritative = false,
                isTruncated = false,
                isRecursionDesired = true,
                isRecursionAvailable = false,
                returnCode = ReturnCode.NO_ERROR
            ),
            1,
            0,
            0,
            0
        ),
        listOf(
            DNSQuestion(
                hostname,
                QueryType.A
            )
        ),
        emptyList(),
        emptyList(),
        emptyList()
    )
}