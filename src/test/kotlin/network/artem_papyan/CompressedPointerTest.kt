package network.artem_papyan

import network.artem_papyan.domain.DNSPacket
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class CompressedPointerTest {

    @Test
    fun compressedPointerEvaluationTest() {
        val offset = 34

        val compressedPointer = DNSPacket.compressedPointer(offset)
        val binaryPointer = compressedPointer.toString(2)

        Assertions.assertTrue(binaryPointer.startsWith(DNSPacket.POINTER_HEADER))

        val offsetFromPointer = binaryPointer.substring(3).toInt(2)

        Assertions.assertEquals(offset, offsetFromPointer)
    }
}