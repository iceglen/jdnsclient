package network.artem_papyan

import io.netty.util.CharsetUtil
import network.artem_papyan.domain.*
import network.artem_papyan.util.DNSUtil
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class ConvertingTest {

    @Test
    fun flagsConvertingTest() {
        val flags = DNSHeaderFlags(
            qr = QR.REQUEST,
            opcode = OPCode.STANDARD_QUERY,
            isAuthoritative = false,
            isTruncated = false,
            isRecursionDesired = true,
            isRecursionAvailable = true,
            returnCode = ReturnCode.NO_ERROR
        )

        val encoded = DNSHeaderFlags.to(flags)
        val decoded = DNSHeaderFlags.from(encoded)

        Assertions.assertEquals(flags, decoded)
    }

    @Test
    fun questionConvertingTest() {
        val questionList = listOf(
            DNSQuestion("hostname1", QueryType.A),
            DNSQuestion("hostname2", QueryType.AAAA),
            DNSQuestion("hostname3", QueryType.AFS),
            DNSQuestion("hostname4", QueryType.ANY),
            DNSQuestion("hostname5", QueryType.APL),
            DNSQuestion("hostname6", QueryType.AXFR),
            DNSQuestion("hostname7", QueryType.CAA)
        )

        val encodedQuestionList = DNSUtil.encodeQuestions(questionList)

        Assertions.assertNotEquals(0, encodedQuestionList.readableBytes())

        val decodedQuestionList = DNSUtil.decodeQuestions(encodedQuestionList, questionList.size)

        Assertions.assertEquals(questionList.size, decodedQuestionList.size)
        Assertions.assertEquals(questionList, decodedQuestionList)
    }

    @Test
    fun resourceRecordConvertingTest() {
        val byteArray = "QWERTYQWERTYQWERTY".toByteArray(CharsetUtil.UTF_8)

        val rrList = listOf(
            ResourceRecord(
                ResourceRecordType.ADDITIONAL,
                "hostname1.com",
                QueryType.A,
                timeToLive = 50,
                resourceDataLength = byteArray.size,
                resourceData = byteArray
            ),
            ResourceRecord(
                ResourceRecordType.ADDITIONAL,
                "hostname2.com",
                QueryType.AAAA,
                timeToLive = 50,
                resourceDataLength = byteArray.size,
                resourceData = byteArray
            ),
            ResourceRecord(
                ResourceRecordType.ADDITIONAL,
                "hostname3.com",
                QueryType.CAA,
                timeToLive = 50,
                resourceDataLength = byteArray.size,
                resourceData = byteArray
            )
        )

        val encodedList = DNSUtil.encodeResourceRecords(rrList)

        Assertions.assertNotEquals(0, encodedList.readableBytes())

        val decodedList = DNSUtil.decodeResourceRecord(encodedList, ResourceRecordType.ADDITIONAL, rrList.size)

        Assertions.assertEquals(rrList.size, decodedList.size)
        Assertions.assertEquals(rrList, decodedList)
    }
}