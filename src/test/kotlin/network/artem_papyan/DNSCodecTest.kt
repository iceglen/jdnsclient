package network.artem_papyan

import io.netty.buffer.ByteBuf
import io.netty.channel.embedded.EmbeddedChannel
import network.artem_papyan.codec.CombinedDNSPacketCodec
import network.artem_papyan.domain.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DNSCodecTest {

    @Test
    fun codecTest() {
        var channel = EmbeddedChannel(CombinedDNSPacketCodec())

        val sampleRequest = this.generateDnsRequest("hostname1.com", QR.REQUEST)

        channel.writeOutbound(sampleRequest)

        val encodedRequest = channel.readOutbound<ByteBuf>()

        Assertions.assertNotNull(encodedRequest)
        Assertions.assertFalse(encodedRequest.readableBytes() == 0)

        channel.finish()

        encodedRequest.retain()

        channel = EmbeddedChannel(CombinedDNSPacketCodec())

        channel.writeInbound(encodedRequest)

        val decodedRequest = channel.readInbound<DNSPacket>()

        Assertions.assertNotNull(decodedRequest)
        Assertions.assertEquals(sampleRequest, decodedRequest)

        Assertions.assertEquals(sampleRequest.questions[0].queryName, decodedRequest.questions[0].queryName)

        channel.finish()

        val sampleResponse = this.generateDnsRequest("hostname2.com", QR.RESPONSE)

        channel = EmbeddedChannel(CombinedDNSPacketCodec())

        channel.writeOutbound(sampleResponse)

        val encodedResponse = channel.readOutbound<ByteBuf>()

        Assertions.assertNotNull(encodedResponse)
        Assertions.assertFalse(encodedResponse.readableBytes() == 0)

        channel.finish()

        encodedResponse.retain()

        channel = EmbeddedChannel(CombinedDNSPacketCodec())

        channel.writeInbound(encodedResponse)

        val decodedResponse = channel.readInbound<DNSPacket>()

        Assertions.assertNotNull(decodedResponse)
        Assertions.assertEquals(sampleResponse, decodedResponse)

        Assertions.assertEquals(sampleResponse.questions[0].queryName, decodedResponse.questions[0].queryName)

        channel.finish()
    }

    private fun generateDnsRequest(hostname: String, qr: QR) = DNSPacket(
        DNSHeader(
            1,
            DNSHeaderFlags(
                qr,
                OPCode.STANDARD_QUERY,
                isAuthoritative = false,
                isTruncated = false,
                isRecursionDesired = true,
                isRecursionAvailable = false,
                returnCode = ReturnCode.NO_ERROR
            ),
            1,
            4,
            0,
            0
        ),
        listOf(
            DNSQuestion(
                hostname,
                QueryType.A
            )
        ),
        listOf(
            ResourceRecord.answer(
                hostname,
                QueryType.A,
                1,
                1500,
                4,
                ByteArray(4)
            ),
            ResourceRecord.answer(
                hostname,
                QueryType.A,
                1,
                1500,
                4,
                ByteArray(4)
            ),
            ResourceRecord.answer(
                hostname,
                QueryType.A,
                1,
                1500,
                4,
                ByteArray(4)
            ),
            ResourceRecord.answer(
                hostname,
                QueryType.A,
                1,
                1500,
                4,
                ByteArray(4)
            )
        ),
        emptyList(),
        emptyList()
    )
}