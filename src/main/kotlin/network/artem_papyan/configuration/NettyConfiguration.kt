package network.artem_papyan.configuration

import io.netty.bootstrap.Bootstrap
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelOption
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioDatagramChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.channel.socket.nio.NioSocketChannel
import network.artem_papyan.domain.DNSPacket
import network.artem_papyan.handler.initializer.DNSClientTCPChannelInitializer
import network.artem_papyan.handler.initializer.DNSClientUDPChannelInitializer
import network.artem_papyan.handler.initializer.DNSServerTCPChannelInitializer
import network.artem_papyan.handler.initializer.DNSServerUDPChannelInitializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object NettyConfiguration {
    private val log: Logger = LoggerFactory.getLogger(this::class.java)

    private val threadCount = 8 * Runtime.getRuntime().availableProcessors()

    private var nioEventLoopGroup: NioEventLoopGroup
    private var executor: ExecutorService

    private lateinit var dnsUdpServerBootstrap: Bootstrap
    private lateinit var dnsTcpServerBootstrap: ServerBootstrap

    init {
        log.info("thread count: $threadCount")

        executor = Executors.newFixedThreadPool(threadCount)
        nioEventLoopGroup = NioEventLoopGroup(threadCount, executor)
    }

    fun nettyInitServer() {
        this.initDnsOverUdpServer()
        this.initDnsOverTcpServer()
    }

    private fun initDnsOverUdpServer() {
        log.info("initializing udp server bootstrap")

        dnsUdpServerBootstrap = Bootstrap()
        dnsUdpServerBootstrap.group(nioEventLoopGroup).channel(NioDatagramChannel::class.java)
            .option(ChannelOption.SO_BROADCAST, true)
            .option(ChannelOption.SO_REUSEADDR, true)
            .handler(DNSServerUDPChannelInitializer())

        val bindFuture = dnsUdpServerBootstrap.bind(53)
        bindFuture.addListener {
            if (it.isSuccess) {
                log.info("udp channel bound, waiting for incoming messages")
            } else {
                log.warn("udp bind attempt failed: ${it.cause().message}")
            }
        }
    }

    private fun initDnsOverTcpServer() {
        log.info("initializing tcp server bootstrap")

        dnsTcpServerBootstrap = ServerBootstrap()
        dnsTcpServerBootstrap.group(nioEventLoopGroup)
            .channel(NioServerSocketChannel::class.java)
            .childOption(ChannelOption.SO_REUSEADDR, true)
            .childHandler(DNSServerTCPChannelInitializer())

        val bindFuture = dnsTcpServerBootstrap.bind(53)
        bindFuture.addListener {
            if (it.isSuccess) {
                log.info("tcp channel bound, waiting for incoming messages")
            } else {
                log.warn("tcp bind attempt failed: ${it.cause().message}")
            }
        }
    }

    fun initDnsOverUdpClient(resultFuture: CompletableFuture<DNSPacket> = CompletableFuture()): Bootstrap {
        log.info("initializing udp client bootstrap")

        val bootstrap = Bootstrap()
        bootstrap.group(nioEventLoopGroup).channel(NioDatagramChannel::class.java)
            .option(ChannelOption.SO_REUSEADDR, true)
            .handler(DNSClientUDPChannelInitializer(resultFuture))

        return bootstrap
    }

    fun initDnsOverTcpClient(resultFuture: CompletableFuture<DNSPacket> = CompletableFuture()): Bootstrap {
        val bootstrap = Bootstrap()
        bootstrap.group(nioEventLoopGroup).channel(NioSocketChannel::class.java)
            .option(ChannelOption.SO_REUSEADDR, true)
            .handler(DNSClientTCPChannelInitializer(resultFuture))

        return bootstrap
    }

    fun gracefulShutdown() {
        log.info("shutting down netty thread pool")
        nioEventLoopGroup.shutdownGracefully()
    }
}