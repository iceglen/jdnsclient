package network.artem_papyan

import network.artem_papyan.configuration.NettyConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import javax.annotation.PreDestroy

@SpringBootApplication
class Application

fun main(args: Array<String>): Unit {
    runApplication<Application>(*args)
    NettyConfiguration.nettyInitServer()
}

@PreDestroy
fun shutdown() = NettyConfiguration.gracefulShutdown()
