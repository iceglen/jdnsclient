package network.artem_papyan.codec.decoder

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.ReplayingDecoder
import network.artem_papyan.codec.decoder.state.DNSPacketDecodeState
import network.artem_papyan.domain.*
import network.artem_papyan.util.DNSUtil
import org.slf4j.LoggerFactory

class DNSPacketDecoder : ReplayingDecoder<DNSPacketDecodeState>(DNSPacketDecodeState.ID) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    private var id: Int = 0
    private var flags: Int = 0
    private var questionCount: Int = 0
    private var answerCount: Int = 0
    private var authorityCount: Int = 0
    private var additionalCount: Int = 0
    private var questionList: MutableList<DNSQuestion> = mutableListOf()
    private var answerList: MutableList<ResourceRecord> = mutableListOf()
    private var authorityList: MutableList<ResourceRecord> = mutableListOf()
    private var additionalList: MutableList<ResourceRecord> = mutableListOf()

    override fun decode(ctx: ChannelHandlerContext, input: ByteBuf, output: MutableList<Any>) {
        val channelId = ctx.channel().id().asLongText()

        val currentState = state()

        log.debug("<$channelId> current decoder state: $currentState")

        when (currentState) {
            DNSPacketDecodeState.ID -> {
                id = input.readUnsignedShort()
                checkpoint(DNSPacketDecodeState.FLAGS)
            }
            DNSPacketDecodeState.FLAGS -> {
                flags = input.readUnsignedShort()
                checkpoint(DNSPacketDecodeState.QUESTION_COUNT)
            }
            DNSPacketDecodeState.QUESTION_COUNT -> {
                questionCount = input.readUnsignedShort()
                checkpoint(DNSPacketDecodeState.ANSWER_COUNT)
            }
            DNSPacketDecodeState.ANSWER_COUNT -> {
                answerCount = input.readUnsignedShort()
                checkpoint(DNSPacketDecodeState.AUTHORITY_COUNT)
            }
            DNSPacketDecodeState.AUTHORITY_COUNT -> {
                authorityCount = input.readUnsignedShort()
                checkpoint(DNSPacketDecodeState.ADDITIONAL_COUNT)
            }
            DNSPacketDecodeState.ADDITIONAL_COUNT -> {
                additionalCount = input.readUnsignedShort()
                checkpoint(DNSPacketDecodeState.QUESTIONS)
            }
            DNSPacketDecodeState.QUESTIONS -> {
                questionList = DNSUtil.decodeQuestions(input, questionCount)
                checkpoint(DNSPacketDecodeState.ANSWERS)
            }
            DNSPacketDecodeState.ANSWERS -> {
                answerList = DNSUtil.decodeResourceRecord(input, ResourceRecordType.ANSWER, answerCount)
                checkpoint(DNSPacketDecodeState.AUTHORITIES)
            }
            DNSPacketDecodeState.AUTHORITIES -> {
                authorityList = DNSUtil
                    .decodeResourceRecord(input, ResourceRecordType.AUTHORITY, authorityCount)
                checkpoint(DNSPacketDecodeState.ADDITIONAL)
            }
            DNSPacketDecodeState.ADDITIONAL -> {
                additionalList = DNSUtil
                    .decodeResourceRecord(input, ResourceRecordType.ADDITIONAL, additionalCount)
                checkpoint(DNSPacketDecodeState.ID)
            }
            else -> throw Error("wrong dns decoder state")
        }

        val readerIdx = input.readerIndex()
        val writerIdx = input.writerIndex()

        if (readerIdx == writerIdx) {
            val dnsHeader = DNSHeader(
                id,
                DNSHeaderFlags.from(flags),
                questionCount,
                answerCount,
                authorityCount,
                additionalCount
            )

            val dnsPacket = DNSPacket(
                dnsHeader,
                questionList,
                answerList,
                authorityList,
                additionalList
            )

            log.info("<$channelId> decoded dns packet: $dnsPacket")

            output.add(dnsPacket)
        }
    }
}