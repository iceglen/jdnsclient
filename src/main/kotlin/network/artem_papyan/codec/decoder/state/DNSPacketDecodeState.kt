package network.artem_papyan.codec.decoder.state

enum class DNSPacketDecodeState {
    ID,
    FLAGS,
    QUESTION_COUNT,
    ANSWER_COUNT,
    AUTHORITY_COUNT,
    ADDITIONAL_COUNT,
    QUESTIONS,
    ANSWERS,
    AUTHORITIES,
    ADDITIONAL
}