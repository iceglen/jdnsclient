package network.artem_papyan.codec

import io.netty.channel.CombinedChannelDuplexHandler
import network.artem_papyan.codec.decoder.DNSPacketDecoder
import network.artem_papyan.codec.encoder.DNSPacketEncoder

class CombinedDNSPacketCodec :
    CombinedChannelDuplexHandler<DNSPacketDecoder, DNSPacketEncoder>(DNSPacketDecoder(), DNSPacketEncoder()) {

}