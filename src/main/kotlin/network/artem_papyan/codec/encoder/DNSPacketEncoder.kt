package network.artem_papyan.codec.encoder

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.handler.codec.MessageToByteEncoder
import network.artem_papyan.domain.DNSHeaderFlags
import network.artem_papyan.domain.DNSPacket
import network.artem_papyan.util.DNSUtil
import org.slf4j.LoggerFactory

class DNSPacketEncoder : MessageToByteEncoder<DNSPacket>() {

    private val log = LoggerFactory.getLogger(this.javaClass)

    override fun encode(ctx: ChannelHandlerContext, msg: DNSPacket, output: ByteBuf) {
        val channelId = ctx.channel().id().asLongText()
        log.info("<$channelId> encoding message: $msg")

        val header = msg.header

        val id = header.id
        output.writeShort(id)

        val flags = header.flags
        output.writeShort(DNSHeaderFlags.to(flags))

        output.writeShort(msg.questions.size)
        output.writeShort(msg.answers.size)
        output.writeShort(msg.authority.size)
        output.writeShort(msg.additionalInformation.size)

        val questionList = msg.questions
        val encodedQuestionList = DNSUtil.encodeQuestions(questionList)
        output.writeBytes(encodedQuestionList)

        val answerList = msg.answers
        val encodedAnswerList = DNSUtil.encodeResourceRecords(answerList)
        output.writeBytes(encodedAnswerList)

        val authorityList = msg.authority
        val encodedAuthorityList = DNSUtil.encodeResourceRecords(authorityList)
        output.writeBytes(encodedAuthorityList)

        val additionalList = msg.additionalInformation
        val encodedAdditionalList = DNSUtil.encodeResourceRecords(additionalList)
        output.writeBytes(encodedAdditionalList)
    }
}