package network.artem_papyan.handler.initializer

import io.netty.channel.Channel
import io.netty.channel.ChannelInitializer
import network.artem_papyan.codec.CombinedDNSPacketCodec
import network.artem_papyan.handler.DNSPacketInboundDatagramHandler
import network.artem_papyan.handler.DNSProxyHandler
import network.artem_papyan.handler.GeneralOutboundDatagramHandler

class DNSServerUDPChannelInitializer : ChannelInitializer<Channel>() {
    override fun initChannel(ch: Channel) {
        val pipeline = ch.pipeline()

        pipeline.addLast(GeneralOutboundDatagramHandler())
        pipeline.addLast(DNSPacketInboundDatagramHandler())
        pipeline.addLast(CombinedDNSPacketCodec())
        pipeline.addLast(DNSProxyHandler())
    }
}