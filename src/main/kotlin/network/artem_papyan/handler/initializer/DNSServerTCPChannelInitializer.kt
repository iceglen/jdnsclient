package network.artem_papyan.handler.initializer

import io.netty.channel.Channel
import io.netty.channel.ChannelInitializer
import network.artem_papyan.codec.CombinedDNSPacketCodec

class DNSServerTCPChannelInitializer : ChannelInitializer<Channel>() {
    override fun initChannel(ch: Channel) {
        val pipeline = ch.pipeline()

        pipeline.addLast(CombinedDNSPacketCodec())
    }
}