package network.artem_papyan.handler.initializer

import io.netty.channel.Channel
import io.netty.channel.ChannelInitializer
import network.artem_papyan.codec.CombinedDNSPacketCodec
import network.artem_papyan.domain.DNSPacket
import network.artem_papyan.handler.ResultHolder
import java.util.concurrent.CompletableFuture

class DNSClientTCPChannelInitializer(
    private val resultFuture: CompletableFuture<DNSPacket> = CompletableFuture()
) : ChannelInitializer<Channel>() {
    override fun initChannel(ch: Channel) {
        val pipeline = ch.pipeline()

        pipeline.addLast(CombinedDNSPacketCodec())
        pipeline.addLast(ResultHolder(resultFuture))
    }
}