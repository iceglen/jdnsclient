package network.artem_papyan.handler

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.socket.DatagramPacket
import io.netty.handler.codec.MessageToMessageEncoder
import java.net.InetAddress
import java.net.InetSocketAddress

class DNSPacketOutboundDatagramHandler(
    private val host: String,
    private val port: Int
) : MessageToMessageEncoder<ByteBuf>() {
    override fun encode(ctx: ChannelHandlerContext, msg: ByteBuf, out: MutableList<Any>) {
        msg.retain()

        out.add(DatagramPacket(msg, InetSocketAddress(InetAddress.getByName(host), port)))
    }
}