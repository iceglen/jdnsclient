package network.artem_papyan.handler

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.channel.socket.DatagramPacket
import network.artem_papyan.util.ConnectionCache

class DNSPacketInboundDatagramHandler : SimpleChannelInboundHandler<DatagramPacket>() {

    override fun channelRead0(ctx: ChannelHandlerContext, msg: DatagramPacket) {
        val channelId = ctx.channel().id().asLongText()

        val content = msg.content()
        content.retain()

        ConnectionCache.cache[channelId] = msg.sender()

        ctx.fireChannelRead(content)
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        val channelId = ctx.channel().id().asLongText()
        ConnectionCache.cache.remove(channelId)
    }
}