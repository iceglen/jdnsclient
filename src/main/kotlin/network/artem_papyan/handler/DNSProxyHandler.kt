package network.artem_papyan.handler

import io.netty.channel.ChannelFuture
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import network.artem_papyan.domain.DNSPacket
import network.artem_papyan.util.NettyClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

class DNSProxyHandler : SimpleChannelInboundHandler<DNSPacket>(false) {

    private val log: Logger = LoggerFactory.getLogger(this::class.java)

    override fun channelRead0(ctx: ChannelHandlerContext, msg: DNSPacket) {
        val channelId = ctx.channel().id().asLongText()

        log.info("<$channelId> proxying dns request: $msg")

        NettyClient.sendUdp("1.1.1.1", msg).whenComplete { result, throwable ->
            if (Objects.nonNull(throwable)) {
                log.error("<$channelId> error while proxying dns request: ${throwable.message}")

                ctx.fireExceptionCaught(throwable)
            } else {
                log.info("<$channelId> response from target server: $result")

                ctx.writeAndFlush(result).addListener {
                    val f = it as ChannelFuture

                    if (f.isDone) {
                        log.info("reply sent to the client")
                    }
                }
            }
        }
    }
}