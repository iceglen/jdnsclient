package network.artem_papyan.handler

import io.netty.buffer.ByteBuf
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.socket.DatagramChannel
import io.netty.channel.socket.DatagramPacket
import io.netty.handler.codec.MessageToMessageEncoder
import network.artem_papyan.util.ConnectionCache
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.net.InetSocketAddress

class GeneralOutboundDatagramHandler : MessageToMessageEncoder<ByteBuf>() {

    private val log: Logger = LoggerFactory.getLogger(this::class.java)

    override fun encode(ctx: ChannelHandlerContext, msg: ByteBuf, out: MutableList<Any>) {
        msg.retain()

        val channel = ctx.channel() as DatagramChannel
        val channelId = channel.id().asLongText()

        val remote = ConnectionCache.cache.remove(channelId) as InetSocketAddress

        log.info("remote: $remote")

        out.add(DatagramPacket(msg, remote))
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        val channelId = ctx.channel().id().asLongText()
        ConnectionCache.cache.remove(channelId)
    }
}