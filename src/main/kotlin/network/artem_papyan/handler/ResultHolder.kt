package network.artem_papyan.handler

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import network.artem_papyan.domain.DNSPacket
import java.util.concurrent.CompletableFuture

class ResultHolder(private val resultFuture: CompletableFuture<DNSPacket> = CompletableFuture<DNSPacket>()) :
    SimpleChannelInboundHandler<DNSPacket>(false) {

    override fun channelRead0(ctx: ChannelHandlerContext, msg: DNSPacket) {
        resultFuture.complete(msg)
        ctx.close()
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        resultFuture.completeExceptionally(cause)
        ctx.close()
    }
}