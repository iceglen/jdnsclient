package network.artem_papyan.util

import io.netty.channel.ChannelFuture
import network.artem_papyan.configuration.NettyConfiguration
import network.artem_papyan.domain.DNSPacket
import network.artem_papyan.handler.DNSPacketOutboundDatagramHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.CompletableFuture

object NettyClient {

    private val log: Logger = LoggerFactory.getLogger(this::class.java)

    private const val dnsDefaultPort = 53

    fun sendUdp(destinationHost: String, dnsPacket: DNSPacket): CompletableFuture<DNSPacket> {
        val resultFuture = CompletableFuture<DNSPacket>()
        val bootstrap = NettyConfiguration.initDnsOverUdpClient(resultFuture)

        val bindFuture = bootstrap.bind(0)
        bindFuture.addListener {
            if (it.isSuccess) {
                log.info("udp channel bound")

                val channelFuture = it as ChannelFuture
                val channel = channelFuture.channel()
                val pipeline = channel.pipeline()

                pipeline.addFirst(DNSPacketOutboundDatagramHandler(destinationHost, dnsDefaultPort))

                channel.writeAndFlush(dnsPacket)
            } else {
                log.warn("udp bind attempt failed: ${it.cause().message}")
                resultFuture.completeExceptionally(it.cause())
            }
        }

        return resultFuture
    }

    fun sendTcp(destinationHost: String, dnsPacket: DNSPacket): CompletableFuture<DNSPacket> {
        val resultFuture = CompletableFuture<DNSPacket>()
        val bootstrap = NettyConfiguration.initDnsOverTcpClient(resultFuture)

        val connectFuture = bootstrap.connect(destinationHost, dnsDefaultPort)
        connectFuture.addListener {
            if (it.isSuccess) {
                log.info("tcp channel bound")

                val channelFuture = it as ChannelFuture
                val channel = channelFuture.channel()

                channel.writeAndFlush(dnsPacket)

                resultFuture.whenComplete { packet, throwable ->
                    if (Objects.nonNull(throwable)) {
                        resultFuture.completeExceptionally(throwable)
                    } else {
                        resultFuture.complete(packet)
                    }
                }
            } else {
                log.warn("tcp bind attempt failed: ${it.cause().message}")
                resultFuture.completeExceptionally(it.cause())
            }
        }

        return resultFuture
    }
}