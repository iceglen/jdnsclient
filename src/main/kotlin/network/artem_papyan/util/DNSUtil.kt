package network.artem_papyan.util

import io.netty.buffer.ByteBuf
import io.netty.buffer.Unpooled
import io.netty.util.CharsetUtil
import network.artem_papyan.domain.*

object DNSUtil {

    private fun decodeDomainName(buf: ByteBuf): String {
        buf.markReaderIndex()

        val possiblePointer = buf.readUnsignedShort()
        val isCompressed = if (DNSPacket.checkIfPointer(possiblePointer)) {
            true
        } else {
            buf.resetReaderIndex()
            false
        }

        val builder = StringBuilder()
        if (!isCompressed) {
            while (true) {
                val bytesAhead = buf.readByte()
                if (bytesAhead == 0.toByte()) {
                    break
                }

                if (builder.isNotEmpty()) {
                    builder.append(".")
                }

                val hostnamePart = buf.readCharSequence(bytesAhead.toInt(), CharsetUtil.UTF_8)
                builder.append(hostnamePart)
            }
        } else {
            val offset = possiblePointer.toString(2).substring(2).toInt(2)
            builder.append(DNSPacket.COMPRESSED_HOSTNAME.replace(DNSPacket.OFFSET_NUM_ALIAS, offset.toString()))
        }

        return builder.toString()
    }

    private fun encodeDomainName(hostname: String): ByteBuf {
        val result = Unpooled.directBuffer()

        if (DNSPacket.COMPRESSED_HOSTNAME_REGEX.matches(hostname)) {
            val offset = DNSPacket.COMPRESSED_HOSTNAME_REGEX.find(hostname)!!.groupValues[1].toInt()
            val pointer = "${DNSPacket.POINTER_HEADER}${offset.toString(2).padStart(14, '0')}".toInt(2)

            result.writeShort(DNSPacket.compressedPointer(pointer))
        } else {
            for (part in hostname.split(".")) {
                result.writeByte(part.length)
                result.writeBytes(part.toByteArray(CharsetUtil.UTF_8))
            }

            result.writeByte(0)
        }

        return result
    }

    fun decodeQuestions(buf: ByteBuf, questionCount: Int): MutableList<DNSQuestion> {
        val questionList = mutableListOf<DNSQuestion>()
        for (i in 0 until questionCount) {
            val hostname = this.decodeDomainName(buf)
            val queryType = buf.readUnsignedShort()
            val queryClass = buf.readUnsignedShort()

            questionList.add(DNSQuestion(hostname, QueryType.valueOfNumVal(queryType), queryClass))
        }

        return questionList
    }

    fun encodeQuestions(questionList: List<DNSQuestion>): ByteBuf {
        val result = Unpooled.directBuffer()

        for (question in questionList) {
            val encodedHostname = this.encodeDomainName(question.queryName)
            result.writeBytes(encodedHostname)

            val queryType = question.queryType.numVal
            result.writeShort(queryType)

            val queryClass = question.queryClass
            result.writeShort(queryClass)
        }

        return result
    }

    fun decodeResourceRecord(
        buf: ByteBuf,
        recordType: ResourceRecordType,
        recordCount: Int
    ): MutableList<ResourceRecord> {
        val result = mutableListOf<ResourceRecord>()

        for (i in 0 until recordCount) {
            val hostname = this.decodeDomainName(buf)
            val queryType = buf.readUnsignedShort()
            val queryClass = buf.readUnsignedShort()
            val timeToLive = buf.readUnsignedInt()
            val resourceDataLength = buf.readUnsignedShort()

            val resourceData = if (!buf.hasArray()) {
                val copy = Unpooled.buffer(resourceDataLength)
                val currentReaderIndex = buf.readerIndex()

                buf.getBytes(currentReaderIndex, copy, 0, resourceDataLength) // it doesn't modify reader index
                buf.readerIndex(currentReaderIndex + resourceDataLength) // we must change it manually

                copy.array()
            } else {
                buf.readBytes(resourceDataLength).array()
            }

            result.add(
                ResourceRecord(
                    recordType,
                    hostname,
                    QueryType.valueOfNumVal(queryType),
                    queryClass,
                    timeToLive.toInt(),
                    resourceDataLength,
                    resourceData
                )
            )
        }

        return result
    }

    fun encodeResourceRecords(resourceRecordList: List<ResourceRecord>): ByteBuf {
        val result = Unpooled.directBuffer()

        for (resourceRecord in resourceRecordList) {
            val encodedHostname = this.encodeDomainName(resourceRecord.domainName)
            result.writeBytes(encodedHostname)

            val queryType = resourceRecord.queryType.numVal
            result.writeShort(queryType)

            val queryClass = resourceRecord.queryClass
            result.writeShort(queryClass)

            val ttl = resourceRecord.timeToLive
            result.writeInt(ttl)

            val dataLength = resourceRecord.resourceDataLength
            result.writeShort(dataLength)

            result.writeBytes(resourceRecord.resourceData)
        }

        return result
    }
}