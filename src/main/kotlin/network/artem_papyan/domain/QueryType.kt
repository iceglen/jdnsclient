package network.artem_papyan.domain

enum class QueryType(val numVal: Int) {
    A(1),
    NS(2),
    CNAME(5),
    SOA(6),
    PTR(12),
    HINFO(13),
    MX(15),
    TXT(16),
    RP(17),
    AFS(18),
    SIG(24),
    KEY(25),
    AAAA(28),
    LOC(29),
    SRV(33),
    NAPTR(35),
    KX(36),
    CERT(37),
    DNAME(39),
    OPT(41),
    APL(42),
    DS(43),
    SSHFP(44),
    IPSECKEY(45),
    RRSIG(46),
    NSEC(47),
    DNSKEY(48),
    DHCID(49),
    NSEC3(50),
    NSEC3PARAM(51),
    TLSA(52),
    SMIMEA(53),
    HIP(55),
    CDS(59),
    CDNSKEY(60),
    OPENPGPKEY(61),
    CSYNC(62),
    ZONEMD(63),
    TKEY(249),
    TSIG(250),
    IXFR(251),
    AXFR(252),
    ANY(255),
    URI(256),
    CAA(257),
    TA(32768),
    DLV(32769);

    companion object {
        fun valueOfNumVal(i: Int): QueryType {
            for (elem in values()) {
                if (elem.numVal == i) {
                    return elem
                }
            }

            throw RuntimeException("insufficient QueryType: $i")
        }
    }
}