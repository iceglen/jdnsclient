package network.artem_papyan.domain

enum class QR(val bitVal: Int) {
    REQUEST(0),
    RESPONSE(1);

    companion object {
        fun valueOfNumVal(i: Int): QR {
            for (elem in values()) {
                if (elem.bitVal == i) {
                    return elem
                }
            }

            throw RuntimeException("insufficient QR")
        }
    }
}