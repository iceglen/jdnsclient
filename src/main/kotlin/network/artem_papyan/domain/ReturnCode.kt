package network.artem_papyan.domain

enum class ReturnCode(val numVal: Int) {
    NO_ERROR(0),
    FORMAT_ERROR(1),
    SERVER_FAILURE(2),
    NAME_ERROR(3),
    NOT_IMPLEMENTED(4),
    REFUSED(5),
    NAME_SHOULD_NOT_EXIST(6),
    RR_SHOULD_NOT_EXIST(7),
    RR_SHOULD_EXIST(8),
    NOT_AUTHORITATIVE_ZONE(9),
    NOT_CONTAINED_IN_ZONE(10),
    BAD_OPT_OR_TSIG(16),
    BAD_KEY(17),
    BAD_TIME_WINDOW(18),
    BAD_TKEY_MODE(19),
    DUPLICATE_KEY_NAME(20),
    BAD_ALGORITHM(21);

    companion object {
        fun valueOfNumVal(i: Int): ReturnCode {
            for (elem in values()) {
                if (elem.numVal == i) {
                    return elem
                }
            }

            throw RuntimeException("insufficient ReturnCode: $i")
        }
    }
}