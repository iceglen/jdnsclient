package network.artem_papyan.domain

data class DNSQuestion(
    val queryName: String,
    val queryType: QueryType,
    val queryClass: Int = 1 // 1 - IP address
)