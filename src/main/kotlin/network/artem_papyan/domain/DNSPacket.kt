package network.artem_papyan.domain

data class DNSPacket(
    val header: DNSHeader,
    val questions: List<DNSQuestion>, // usually contains only 1 element
    val answers: List<ResourceRecord>,
    val authority: List<ResourceRecord>,
    val additionalInformation: List<ResourceRecord>
) {
    companion object {
        const val POINTER_HEADER = "11"
        const val OFFSET_NUM_ALIAS = "OFFSET_NUM"
        const val COMPRESSED_HOSTNAME = "COMPRESSED (offset=$OFFSET_NUM_ALIAS)"

        val COMPRESSED_HOSTNAME_REGEX = "^COMPRESSED \\(offset=(\\d*)\\)\$".toRegex()

        private const val COMPRESSED_POINTER = 0xC0

        fun compressedPointer(offset: Int): Int {
            val builder = StringBuilder(16)

            builder.append(COMPRESSED_POINTER.toString(2))

            builder.append(offset.toString(2).padStart(8, '0'))

            return builder.toString().toInt(2)
        }

        fun checkIfPointer(headerPart: Int): Boolean {
            val binaryHeader = headerPart.toString(2).padStart(16, '0')
            val possiblePointer = binaryHeader.substring(0..1)

            return possiblePointer == POINTER_HEADER
        }
    }
}
