package network.artem_papyan.domain

data class DNSHeader(
    val id: Int,
    val flags: DNSHeaderFlags,
    val questionCount: Int,
    val answerCount: Int,
    val authorityCount: Int,
    val additionalCount: Int
)

data class DNSHeaderFlags(
    val qr: QR, // QR
    val opcode: OPCode, // opcode
    val isAuthoritative: Boolean, // AA
    val isTruncated: Boolean, // TC
    val isRecursionDesired: Boolean, // RD
    val isRecursionAvailable: Boolean, // RA
    val returnCode: ReturnCode // rcode
) {
    companion object {
        fun from(input: Int): DNSHeaderFlags {
            val flags = input.toString(2).padStart(16, '0')

            val qr = QR.valueOfNumVal(Integer.parseInt(flags[0].toString()))
            val opcode = OPCode.valueOfNumVal(flags.substring(1, 5).toInt(2))
            val isAuthoritative = flags[5] == '1'
            val isTruncated = flags[6] == '1'
            val isRecursionDesired = flags[7] == '1'
            val isRecursionAvailable = flags[8] == '1'
            val returnCode = ReturnCode.valueOfNumVal((flags.substring(12, 16).toInt(2)))

            return DNSHeaderFlags(
                qr,
                opcode,
                isAuthoritative,
                isTruncated,
                isRecursionDesired,
                isRecursionAvailable,
                returnCode
            )
        }

        fun to(flags: DNSHeaderFlags): Int {
            val resultBuilder = StringBuilder()

            resultBuilder.append(flags.qr.bitVal)
            resultBuilder.append(flags.opcode.numVal.toString(2).padStart(4, '0'))
            resultBuilder.append(if (flags.isAuthoritative) 1 else 0)
            resultBuilder.append(if (flags.isTruncated) 1 else 0)
            resultBuilder.append(if (flags.isRecursionDesired) 1 else 0)
            resultBuilder.append(if (flags.isRecursionAvailable) 1 else 0)
            resultBuilder.append("000")
            resultBuilder.append(
                if (flags.returnCode.numVal > 15)
                    ReturnCode.NO_ERROR
                else
                    flags.returnCode.numVal
                        .toString(2)
                        .padStart(4, '0')
            )

            return resultBuilder.toString().toInt(2)
        }
    }
}