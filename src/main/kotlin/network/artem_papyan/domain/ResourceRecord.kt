package network.artem_papyan.domain

data class ResourceRecord(
    val recordType: ResourceRecordType, // custom
    val domainName: String,
    val queryType: QueryType,
    val queryClass: Int = 1,
    val timeToLive: Int, // TTL
    val resourceDataLength: Int,
    val resourceData: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ResourceRecord

        if (recordType != other.recordType) return false
        if (domainName != other.domainName) return false
        if (queryType != other.queryType) return false
        if (queryClass != other.queryClass) return false
        if (timeToLive != other.timeToLive) return false
        if (resourceDataLength != other.resourceDataLength) return false
        if (!resourceData.contentEquals(other.resourceData)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = recordType.hashCode()
        result = 31 * result + domainName.hashCode()
        result = 31 * result + queryType.hashCode()
        result = 31 * result + queryClass
        result = 31 * result + timeToLive
        result = 31 * result + resourceDataLength
        result = 31 * result + resourceData.contentHashCode()
        return result
    }

    companion object {
        fun answer(
            domainName: String,
            queryType: QueryType,
            queryClass: Int = 1,
            timeToLive: Int,
            resourceDataLength: Int,
            resourceData: ByteArray
        ) = ResourceRecord(
            ResourceRecordType.ANSWER,
            domainName,
            queryType,
            queryClass,
            timeToLive,
            resourceDataLength,
            resourceData
        )

        fun authority(
            domainName: String,
            queryType: QueryType,
            queryClass: Int = 1,
            timeToLive: Int,
            resourceDataLength: Int,
            resourceData: ByteArray
        ) = ResourceRecord(
            ResourceRecordType.AUTHORITY,
            domainName,
            queryType,
            queryClass,
            timeToLive,
            resourceDataLength,
            resourceData
        )

        fun additional(
            domainName: String,
            queryType: QueryType,
            queryClass: Int = 1,
            timeToLive: Int,
            resourceDataLength: Int,
            resourceData: ByteArray
        ) = ResourceRecord(
            ResourceRecordType.ADDITIONAL,
            domainName,
            queryType,
            queryClass,
            timeToLive,
            resourceDataLength,
            resourceData
        )
    }
}