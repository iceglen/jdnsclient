package network.artem_papyan.domain

enum class OPCode(val numVal: Int) {
    STANDARD_QUERY(0),
    INVERSE_QUERY(1),
    SERVER_STATUS_REQUEST(2),
    NOTIFY(4),
    UPDATE(5);

    companion object {
        fun valueOfNumVal(i: Int): OPCode {
            for (elem in values()) {
                if (elem.numVal == i) {
                    return elem
                }
            }

            throw RuntimeException("insufficient OPCode")
        }
    }
}