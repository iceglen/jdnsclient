package network.artem_papyan.domain

enum class ResourceRecordType { ANSWER, AUTHORITY, ADDITIONAL }